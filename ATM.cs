﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtmApp
{
    public class ATM
    {
        public ATM(int balance, User user)
        {
            this.CurrentBalance = balance;
            this.Customer = user;
        }
        public int CurrentBalance { get; set; }

        public Language Language { get; set; }

        public User Customer { get; set; }



        public void SelectLanguage()
        {
            StringBuilder languageMenu = new StringBuilder();
            languageMenu.AppendLine("Please choose your language");
            languageMenu.AppendLine("1. English");
            languageMenu.AppendLine("2. Igbo");
            languageMenu.AppendLine("3. Hausa");
            languageMenu.AppendLine("4. Yoruba");
            Console.WriteLine(languageMenu.ToString());

            string selectedLanguage = Console.ReadLine();

            Language atmLanguage;

            switch (selectedLanguage)
            {
                case "1": // English
                    atmLanguage = Language.English;
                    break;
                case "2": // Igbo
                    atmLanguage = Language.Igbo;
                    break;
                case "3": // Hausa
                    atmLanguage = Language.Hausa;
                    break;
                case "4": // Yoruba
                    atmLanguage = Language.Yoruba;
                    break;

                default:
                    atmLanguage = Language.English;
                    break;
            }

            this.Language = atmLanguage;

        }
        public bool RequstAndValidatePin()
        {
            Console.WriteLine("Please insert your PIN");
            string enteredPin = Console.ReadLine();

            // TODO: Validate the pin
            if (enteredPin.Length != 4)
            {
                throw new Exception("Invalid Pin");
            }

            if (enteredPin != this.Customer.Pin)
            {
                throw new Exception("Incorrect Pin");

            }
            return true;
        }


        public void DisplayMenu()
        {
            StringBuilder atmMenu = new StringBuilder();
            atmMenu.AppendLine("Menu Option");
            atmMenu.AppendLine("1: WithDraw");
            atmMenu.AppendLine("2: Account Balance");
            atmMenu.AppendLine("3: Deposit");
            Console.WriteLine(atmMenu.ToString());

            string selectedMenu = Console.ReadLine();


            switch (selectedMenu)
            {
                case "1":
                    this.WithDraw();

                    break;

                case "2":
                    this.DisplayAccountBalance();
                    break;

                case "3":
                    this.CollectDeposit();
                    break;

            }

        }

        public void WithDraw()
        {
            StringBuilder atmMenu = new StringBuilder();
            atmMenu.AppendLine("Please enter withdraw amount");
            atmMenu.AppendLine("This machine dispenses in 500 - 1000");
            Console.WriteLine(atmMenu.ToString());
            var enteredAmount = Console.ReadLine();
            int amount;

            // check that the values is a number
            if (!int.TryParse(enteredAmount, out amount))
            {
                throw new Exception("Amount must be a number");
            }

            if (!(amount % 500 == 0))
            {
                throw new Exception("Amount must be divisible by 500 or 1000");
            }

            if (amount > this.Customer.AccountBalance)
            {
                throw new Exception("Insufficient funds ");
            }

            if (amount > this.CurrentBalance)
            {
                throw new Exception("Temporarily unable to dispense cash ");
            }

            this.CurrentBalance -= amount;
            this.Customer.AccountBalance -= amount;

            Console.WriteLine("Please take your cash");

            this.Pause();
            this.DisplayMenu();
        }

        public void DisplayAccountBalance()
        {
            Console.WriteLine($"Your account balance is {this.Customer.AccountBalance}");
            this.Pause();
            this.DisplayMenu();

        }

        public void CollectDeposit()
        {

            Console.WriteLine("How much do you want to deposit");
            string enterAmount = Console.ReadLine();
            int depositAmount;

            if (!int.TryParse(enterAmount, out depositAmount))
            {
                throw new Exception("Enter amount must be a Number");
            }

            this.CurrentBalance += depositAmount;
            this.Customer.AccountBalance += depositAmount;

            Console.WriteLine($"Your account has been credited with: {depositAmount}");

            this.Pause();
            this.DisplayMenu();

        }
        public void Pause()
        {
            Console.WriteLine("Press any key to continue");
            Console.ReadKey(true);
        }
    }

}
